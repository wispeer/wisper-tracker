# wispeer

Wispeer is an decentralized and distributed social network.

There are multiple git repo :

**wispeer-client**: this repo. This is the implementation os client-side wipeer protocol. It's the library you may want to use if you want to create your own client (as a website or a bot for instance). This can also be used for other network aplications.

**wispeer-tracker**: https://framagit.org/wispeer/wisper-tracker This is the tracker code implementing the server-side wispeer protocol. If you want to host a tracker (aka tag) you may use this.

**wispeer-gui**: https://framagit.org/wispeer/wispeer-gui Contains the code of the wispeer gui using angular 4 and bootstrap 4.

**wispeer-www-build**: https://framagit.org/wispeer/wispeer-www-build Contains build of wispeer-gui. If you want to host a copy of the website this is your repo.

## wispeer tracker

If you want to make an instance of a tracker you should

1. clone this repo
2. run npm install
3. make a wss reverse-proxy redirection to a port p (eg: 8000)
4. start the tracker with ```node index.js -p 8000```

You can run node index.js multiple time with different port and different reverse-proxy addresses to make multiple trackers.