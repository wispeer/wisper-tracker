var WebSocketServer = require('websocket').server
var http = require('http')
var argv = require('argv')

var options = {
  name: 'port',
  short: 'p',
  type: 'int',
  description: 'websocket listen port',
  example: "'node index.js -p 8080'"
}

var args = argv.option(options).run()
var port = args.options.port
if (!port) {
  port = 8080
}

var offers = []
var connectionInProgress = {}
var nbOffersToSend = 10
var lastId = -1

var server = http.createServer(function (request, response) {
  console.log((new Date()) + ' Received request for ' + request.url)
  response.writeHead(404)
  response.end()
})
server.listen(port, function () {
  console.log((new Date()) + ' Server is listening on port ' + port)
})

var wsServer = new WebSocketServer(
  {
    httpServer: server,
    autoAcceptConnections: false
  })

function originIsAllowed (origin) {
    // put logic here to detect whether the specified origin is allowed.
  return true
}

function getNewId () {
  lastId++
  return lastId
}

function sendSomeOffers (connection) {
  var i = 0
  var nbOfferSent = 0
        // never send more than half the offers to keep some for others
  var limitedNbOffersToSend = Math.min(nbOffersToSend, Math.floor(offers.length / 2 + 1))
  while (i < limitedNbOffersToSend && offers.length > 0) {
    var offerIndex = Math.floor(Math.random() * (offers.length - 1))
    var offer = offers[offerIndex]
    if (offer.connection.id === connection.id) {
      console.log('same ID, I keep this one')
    } else {
      offers.splice(offerIndex, 1)
      sendOffer(connection, offer.data, offer.connection.id, false)
      nbOfferSent++
      connectionInProgress[offer.connection.id] = offer.connection
      console.log('add connection with ID=' + offer.connection.id)
    }
    i++
  }
  console.log('sent ' + nbOfferSent + ' offers!')
}

function getConnectionWithTarget (target) {
  console.log('get connection with ID=' + target)
  return connectionInProgress[target]
}

function sendOffer (connection, offer, id, spontaneous) {
  if (spontaneous) {
    connection.sendUTF(offer)
  } else {
    connection.sendUTF(id + '-' + offer)
  }
}

wsServer.on('request', function (request) {
  if (!originIsAllowed(request.origin)) {
        // Make sure we only accept requests from an allowed origin
    request.reject()
    console.log((new Date()) + ' Connection from origin ' + request.origin + ' rejected.')
    return
  }

  if (request.requestedProtocols[0] !== 'wis-peer') {
    console.log('Wrong domain!')
    request.reject()
    return
  }

  var connection = request.accept('wis-peer', request.origin)
  connection.id = getNewId()
  console.log((new Date()) + ' Connection accepted.')
  sendSomeOffers(connection)
  connection.on('message', function (message) {
    if (message.type === 'utf8') {
      console.log('Received Message: ' + message.utf8Data)
      var messageSplited = message.utf8Data.split('-')
      var command = messageSplited[0]
      if (command === '0') {
        console.log('new offer from id ' + connection.id)
        var offLen = offers.length
        console.log('nb Offers: ' + (offLen + 1))
        for (var i = 0; i < offLen; i++) {
          if (offers[i].connection.id === connection.id) {
            offLen = i
          }
        }
        offers[offLen] = {}
        offers[offLen].data = messageSplited[1]
        offers[offLen].connection = connection
      } else if (command === '1') {
        var target = messageSplited[1]
        var offer = messageSplited[2]
        sendOffer(getConnectionWithTarget(target), offer, connection.id, true)
      } else if (command === '2') {
        sendSomeOffers(connection)
      }
    } else if (message.type === 'binary') {
      console.log('Received Binary Message of ' + message.binaryData.length + ' bytes')
    }
  })
  connection.on('close', function (reasonCode, description) {
    console.log((new Date()) + ' Peer ' + connection.remoteAddress + ' disconnected.')
    for (var i = 0; i < offers.length && offers.length > 0; i++) {
      if (offers[i].connection.id === connection.id) {
        offers.splice(i, 1)
        console.log('remove offer n°' + i + ' offers remainings: ' + offers.length)
        i--
      }
    }
  })
})
